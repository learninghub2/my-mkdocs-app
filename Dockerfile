FROM squidfunk/mkdocs-material

WORKDIR /docs

COPY . /docs

EXPOSE 8080

CMD [ "serve", "--dev-addr=0.0.0.0:8080" ]