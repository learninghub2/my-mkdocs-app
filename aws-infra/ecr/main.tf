
provider "aws" {
  region = "ap-southeast-1"
  access_key = var.access_key
  secret_key = var.secret_key
}

terraform {
  backend "s3" {
    bucket = "121223-tf-state-1"
    key = "ecr.tfstate"
    region = "ap-southeast-1"
  }
}

resource "aws_ecr_repository" "ecr" {
  name = "gitlab-ecr-tutorial"
  image_tag_mutability = "MUTABLE"
  tags = {
    name = "gitlab-ecr-tutorial"
    createdBy = "terraform"
  }
  image_scanning_configuration {
    scan_on_push = true
  }

}

resource "aws_ecr_repository_policy" "public_policy" {
  repository = aws_ecr_repository.ecr.name
  policy = jsonencode({
    version = "2008-10-17"
    statement = [
      {
        sid = "AllowPull"
        effect = "Allow"
        principal = "*"
        actions = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability",
          "ecr:PutImage",
          "ecr:InitiateLayerUpload",
          "ecr:UploadLayerPart",
          "ecr:CompleteLayerUpload",
          "ecr:DescribeRepositories",
          "ecr:GetRepositoryPolicy",
          "ecr:ListImages",
          "ecr:DeleteRepository",
          "ecr:BatchDeleteImage",
          "ecr:SetRepositoryPolicy",
          "ecr:DeleteRepositoryPolicy",
        ]
      }
    ]
  })
}

output "repository_url" {
  value = aws_ecr_repository.ecr.repository_url
}